﻿#define PARALLEL
#define BENCHMARK

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace MandelbrotSet
{
    class Program
    {
        public struct Complex
        {
            private double _real;
            private double _imaginary;

            public Complex(double x, double y)
            {
                _real = x;
                _imaginary = y;
            }
            public double Real
            {
                get
                {
                    return _real;
                }
                set
                {
                    _real = value;
                }
            }

            public double Imaginary
            {
                get
                {
                    return _imaginary;
                }

                set
                {
                    _imaginary = value;
                }
            }

        }
        static void Main(string[] args)
        {

            double[,] pixelMatrix = new double[MandelbrotSet.IMG_HEIGHT, MandelbrotSet.IMG_WIDTH];
            MandelbrotSet MSet = new MandelbrotSet();
            double dx = MSet.Width / MandelbrotSet.IMG_WIDTH;
            double dy = MSet.Height / MandelbrotSet.IMG_HEIGHT;

#if PARALLEL
            ParallelOptions po = new ParallelOptions
            {
                MaxDegreeOfParallelism = Environment.ProcessorCount
            };
#endif
#if BENCHMARK
            Stopwatch watch = new Stopwatch();
            string filePath = "c:\\benchMarkM.txt";
            using (StreamWriter sw = new StreamWriter(filePath, true))
            {
                string description = "\n===== Benchmarking MandelbrotSet =====\n";
                sw.WriteLine(description);
                // clean up
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                DateTime now = DateTime.Now;
                sw.WriteLine(now);
                sw.WriteLine("Number of cores : {0}",Environment.ProcessorCount);
                watch.Start();
#endif

#if PARALLEL
                sw.WriteLine("Parallel: ");
                Parallel.For(0, (long)MandelbrotSet.IMG_HEIGHT, y =>
#endif
#if SERIAL
                sw.WriteLine("Serial: ");
                for (int y = 0; y < MandelbrotSet.IMG_HEIGHT; y++)
#endif
                {
                    for (int x = 0; x < MandelbrotSet.IMG_WIDTH; x++)
                    {
                        double r = MSet.MinReal + x * dx;
                        double i = MSet.MinImg + y * dy;
                        pixelMatrix[y, x] = ComputeMandColor(r, i);
                    }
                }
#if PARALLEL
);
#endif

#if BENCHMARK
                watch.Stop();
                sw.WriteLine("Time Elapsed {0} ms\n", watch.Elapsed.TotalMilliseconds);
            }
                            WritePixels(pixelMatrix);
#endif
        }

        static int ComputeMandColor(double r, double i)
        {
            Complex cplx = new Complex(0, 0);
            int count = 0;
            double abs_sq, tmp;
            do
            {
                tmp = (cplx.Real * cplx.Real) - (cplx.Imaginary * cplx.Imaginary) + r;
                cplx.Imaginary = 2 * (cplx.Real * cplx.Imaginary) + i;
                cplx.Real = tmp;

                abs_sq = (cplx.Real * cplx.Real) + (cplx.Imaginary * cplx.Imaginary);
                count++;
            } while ((abs_sq < 4) && (count < 128));

            return count;
        }

        static bool WritePixels(double[,] pixels)
        {
            string fileName = "c:\\pixels";
            try
            {
                using (System.IO.BinaryWriter bs = new BinaryWriter(File.Open(fileName, FileMode.Create)))
                {
                    int counter = 0;
                    for (int i = 0; i < pixels.GetLength(0); i++)
                    {
                        for (int j = 0; j < pixels.GetLength(1); j++)
                        {
                            bs.Write(pixels[i, j]);
                            counter++;
                        }
                    }
                    bs.Close();
                    Console.WriteLine(counter);
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }


}
