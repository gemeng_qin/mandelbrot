﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    class MandelbrotSet
    {
        public MandelbrotSet()
        {
            MinReal = -2.0;
            MaxReal = 2.0;
            MinImg = -2.0;
            MaxImg = 2.0;
        }

        public const int IMG_WIDTH = 8000;
        public const int IMG_HEIGHT = 6000;
        private double _minReal;
        private double _maxReal;
        private double _minImaginary;
        private double _maxImaginary;

        public double MinReal
        {
            get
            {
                return _minReal;
            }
            set
            {
                _minReal = value;
            }
        }

        public double MaxReal
        {
            get
            {
                return _maxReal;
            }
            set
            {
                _maxReal = value;
            }
        }

        public double MinImg
        {
            get
            {
                return _minImaginary;
            }
            set
            {
                _minImaginary = value;
            }
        }

        public double MaxImg
        {
            get
            {
                return _maxImaginary;
            }
            set
            {
                _maxImaginary = value;
            }
        }
        public double Width
        {
            get
            {
                return MaxReal - MinReal;
            }
        }
        public double Height
        {
            get
            {
                return MaxImg - MinImg;
            }
        }


    }

}
